import 'package:flutter/material.dart';
import 'package:safe_rides/AdminPage/appbar.dart';
import 'package:safe_rides/DriverPage/appBar.dart';
import 'package:safe_rides/UserPage/appbar.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import './Authentication/auth_page.dart';
import 'routes.dart';

class UserDetails extends StatelessWidget {
  final Map<String, dynamic> user;
  final TextEditingController commentsController;

  const UserDetails(this.user, this.commentsController);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: user['userType'] == 'user'
          ? userAppBar(context, user, commentsController, false)
          : user['userType'] == 'admin'
              ? adminAppBar(context, false)
              : driverAppBar(context, false, user),
      body: SingleChildScrollView(
        child: Container(
            color: Colors.grey[200],
            constraints: BoxConstraints(
                minHeight: MediaQuery.of(context).size.height - 80.0,
                minWidth: MediaQuery.of(context)
                    .size
                    .width), // Reducing AppBar Height
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width / 15,
                vertical: MediaQuery.of(context).size.width / 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height / 3.0,
                  child: Center(
                    child: Icon(
                      Icons.account_circle,
                      size: 200.0,
                      color: Theme.of(context).colorScheme.onSurface,
                    ),
                  ), // Can place image instead of Icon
                ),
                Column(
                  children: <Widget>[
                    Text(
                      user['name'],
                      style:
                          TextStyle(fontSize: 24.0, color: Colors.yellow[900]),
                    ),
                    //   Text(
                    //   user['id'],
                    //    style: TextStyle(fontSize: 12.0, color: Colors.black),
                    // ),
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Contact No. :',
                      style: TextStyle(fontSize: 16.0, color: Colors.black),
                    ),
                    Text(
                      user['phoneNoFromRouteDB'],
                      style: TextStyle(fontSize: 16.0, color: Colors.black),
                    )
                  ],
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Emergency Contacts :',
                      style: TextStyle(fontSize: 16.0, color: Colors.black),
                    ),
                    Column(
                      children: <Widget>[
                        user['emergencyContact'][0] != null
                            ? Text(
                                user['emergencyContact'][0],
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.black),
                              )
                            : Container(),
                        user['emergencyContact'][1] != null
                            ? Text(
                                user['emergencyContact'][1],
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.black),
                              )
                            : Container()
                      ],
                    )
                  ],
                ),
              ],
            )),
      ),
      //   --> use one of the childs for verification purpose
    );
  }
}
