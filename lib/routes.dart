class Routes{
     static const String USER_AUTH = 'http://10.150.120.255:4000/users/authenticate';             // User Authentication route    --> Using firestore instead
     static const String ROUTES_DATA = 'http://172.17.251.129:3000/routes';                          // Routes Data
     static const String USERS_DATA = 'http://172.17.251.129:3000/users';                          // Users Data
     static const String TRIPS_DATA = 'http://172.17.251.129:3000/trips';                      // trips Data
     
     static const REGISTER_PAGE = '/registerPage';
     static const USER_HOME = '/userHome';
     static const ADMIN_HOME = '/adminHome';
     static const DRIVER_HOME = '/driverHome';
     static const DEFAULT_ROUTE = '/';
     static const USER_PROFILE = '/userProfile';
     static const BOARDED_USERS = '/boardedUsers';
     static const PICKUP_LOCATIONS = '/pickupLocations';

     static const USERS_COLLECTION = 'userDataV7';
     static const TRIPSS_COLLECTION = 'tripsDataV7';
     
                             
}