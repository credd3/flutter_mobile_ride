import 'package:flutter/material.dart';

import './auth_page.dart';

class Registerpage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterPageState();
  }
}

class RegisterPageState extends State<Registerpage> {
  String _adminName;
  String _password;
  String _organizationName;
  String _phoneNumber;
  String _emailId;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          leading: Container(
              padding: EdgeInsets.all(10.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Image.asset(
                  'assets/sapeRidesLogo.png',
                  color: Colors.white,
                ),
              )),
          title: Text(
            "Safe Rides Register",
          ),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: Container(
            margin: EdgeInsets.only(left: 20.0, right: 20.0),
            child: Center(
              child: SingleChildScrollView(
                  child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 10.0),
                    child: Image.asset(
                      'assets/sapeRidesLogo.png',
                      height: 100.0,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 20.0),
                    child:Text('Join the community')),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return "Name is required";
                            }
                            return null;
                          },
                          onSaved: (String adminName) {
                            setState(() {
                              _adminName = adminName;
                            });
                          },
                          style: TextStyle(fontWeight: FontWeight.w300),
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                              icon: Icon(Icons.person), labelText: 'Name'),
                        ),
                        TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return "Password is required";
                            }
                            return null;
                          },
                          onSaved: (String password) {
                            _password = password;
                          },
                          style: TextStyle(fontWeight: FontWeight.w300),
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          decoration: InputDecoration(
                              icon: Icon(Icons.security), labelText: 'Password'),
                        ),
                        TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return "Please provide your organization Name";
                            }
                            return null;
                          },
                          onSaved: (String organizationName) {
                            organizationName = organizationName;
                          },
                          style: TextStyle(fontWeight: FontWeight.w300),
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          decoration: InputDecoration(
                              icon: Icon(Icons.business), labelText: 'Organization Name'),
                        ),
                        TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return "Phone Number is required";
                            }
                            return null;
                          },
                          onSaved: (String phoneNumber) {
                            _phoneNumber = phoneNumber;
                          },
                          style: TextStyle(fontWeight: FontWeight.w300),
                          keyboardType: TextInputType.number,
                          obscureText: true,
                          decoration: InputDecoration(
                              icon: Icon(Icons.phone), labelText: 'Phone Number'),
                        ),
                        TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return "Email ID is required";
                            }
                            return null;
                          },
                          onSaved: (String emailId) {
                            _emailId = emailId;
                          },
                          style: TextStyle(fontWeight: FontWeight.w300),
                          keyboardType: TextInputType.emailAddress,
                          obscureText: true,
                          decoration: InputDecoration(
                              icon: Icon(Icons.email), labelText: 'Email'),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(
                              top: 40.0,
                            ),
                            child: MaterialButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(
                                      MediaQuery.of(context).size.height)),
                              elevation: 5.0,
                              onPressed: () {
                                _formKey.currentState.save();
                                if (!_formKey.currentState.validate()) {
                                  return;
                                }
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                "Register Organization",
                                style: TextStyle(fontFamily: "Oswald"),
                              ),
                              color: Theme.of(context).primaryColor,
                            )),
                      ],
                    ),
                  ),
                  SizedBox(height: 20.0,),
                ],
              )),
            ),
            ),
      ),
    );
  }
}
