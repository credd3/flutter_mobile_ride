import 'package:flutter/material.dart';
//import 'package:http/http.dart' as http;
//import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';

import '../routes.dart';
import './register_page.dart';

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AuthPageState();
  }
}

class AuthPageState extends State<AuthPage> {
  String _email;
  String _passowrd;
  String errMsg;
  bool isLoading = false;
  bool showAuthMessage = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // Future<bool> setToken(String token) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   //prefs.setString(key, value)
  // }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          leading: Container(
            padding: EdgeInsets.all(10.0),
            child: Image.asset(
              'assets/sapeRidesLogo.png',
              color: Colors.white,
            ),
          ),
          title: Text(
            "Safe Rides Login",
          ),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: Stack(
         children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 20.0, right: 20.0),
                child: Center(
                  child: SingleChildScrollView(
                      child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 20.0),
                        child: Image.asset(
                          'assets/sapeRidesLogo.png',
                          height: 100.0,
                        ),
                      ),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return "Email is required";
                                }
                                return null;
                              },
                              onSaved: (String email) {
                                setState(() {
                                  _email = email;
                                });
                              },
                              style: TextStyle(fontWeight: FontWeight.w300),
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                  icon: Icon(Icons.email), labelText: 'Email'),
                            ),
                            TextFormField(
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return "Password is required";
                                }
                                return null;
                              },
                              onSaved: (String password) {
                                _passowrd = password;
                              },
                              style: TextStyle(fontWeight: FontWeight.w300),
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              decoration: InputDecoration(
                                  icon: Icon(Icons.security),
                                  labelText: 'Password'),
                            ),
                            Container(
                                padding: EdgeInsets.only(top: 10.0),
                                child: showAuthMessage
                                    ? Text(
                                         errMsg,
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontFamily: "Oswald"),
                                            textAlign: TextAlign.center,
                                      )
                                    : SizedBox()),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.only(
                                  top: 40.0,
                                ),
                                child: MaterialButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          MediaQuery.of(context).size.height)),
                                  elevation: 5.0,
                                  onPressed: () {
                                    // // Navigator.of(context).pushNamedAndRemoveUntil(Routes.ADMIN_HOME, (Route route) => route == null);
                                    //  Firestore.instance.collection(Routes.USERS_COLLECTION).document('136428').get().then((onValue){
                                    //    print(onValue.data);
                                    //    Navigator.of(context).pushNamedAndRemoveUntil(Routes.USER_HOME, (Route route) => route == null, arguments: {'user' : onValue.data});
                                    // });
                                    _formKey.currentState.save();
                                    if (!_formKey.currentState.validate()) {
                                      return;
                                    }
                                    setState(() {
                                          isLoading = true;
                                        });
                                    FirebaseAuth.instance.signInWithEmailAndPassword(email: _email,password: _passowrd).then((onValue){
                                      Firestore.instance.collection(Routes.USERS_COLLECTION).where('emailId',isEqualTo:_email).getDocuments().then((onValue){
                                        final Map<String, dynamic> user = onValue.documents[0].data;
                                        switch (user['userType']) {
                                          case 'user':
                                            Navigator.of(context).pushNamedAndRemoveUntil(Routes.USER_HOME, (Route route) => route == null, arguments: {'user' : user});
                                            break;
                                          case 'admin':
                                            Navigator.of(context).pushNamedAndRemoveUntil(Routes.ADMIN_HOME, (Route route) => route == null);
                                            break;
                                          case 'driver':
                                            Navigator.of(context).pushNamedAndRemoveUntil(Routes.DRIVER_HOME, (Route route) => route == null, arguments: {'user' : user});
                                            break;
                                          default:
                                            setState((){
                                              isLoading = false;
                                              showAuthMessage = true;
                                              errMsg = 'Something is Wrong..Please try again';
                                            });
                                        }
                                     });
                                    }).catchError((error){
                                      setState(() {
                                        isLoading = false;
                                        showAuthMessage = true;
                                        errMsg = error.message; 
                                      });
                                    });
                                  },
                                  child: Text(
                                    "Login",
                                    style: TextStyle(fontFamily: "Oswald"),
                                  ),
                                  color: Theme.of(context).primaryColor,
                                )),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.only(
                                  top: 10.0,
                                ),
                                child: FlatButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          MediaQuery.of(context).size.height)),
                                  onPressed: () {
                                         Navigator.of(context).pushNamed(Routes.REGISTER_PAGE);
                                  },
                                  child: Text(
                                    "Register your Organization ?",
                                  ),
                                ))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                    ],
                  )),
                ),
              ),
              isLoading
            ? Container(
              color: Colors.black54,
              child: Center(child:CircularProgressIndicator()),
            )
            : Container(),
         ],
        )
      ),
    );
  }
}
