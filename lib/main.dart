import 'package:flutter/material.dart';
import 'package:safe_rides/DriverPage/pickup_loactions.dart';
import 'package:safe_rides/user_details.dart';

import 'Authentication/auth_page.dart';
import 'AdminPage/admin_page.dart';
import 'Authentication/register_page.dart';
import 'DriverPage/driver_page.dart';
import 'UserPage/user_page.dart';
import 'AdminPage/boarded_users.dart';

import 'routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "hey",
      home: AuthPage(),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      onGenerateRoute: (settings) {
        final Map<String, dynamic> arguments = settings.arguments;
        switch (settings.name) {
          case Routes.REGISTER_PAGE:
            print(arguments);
            return MaterialPageRoute(builder: (context) {
              return Registerpage();
            });
            break;
          case Routes.USER_HOME:
            return MaterialPageRoute(builder: (context) {
              return UserPage(arguments['user']);
            });
            break;
          case Routes.ADMIN_HOME:
            return MaterialPageRoute(builder: (context) {
              return AdminPage();
            });
            break;
          case Routes.DRIVER_HOME:
            return MaterialPageRoute(builder: (context) {
              return DriverPage(arguments['user']);
            });
            break;
          case Routes.USER_PROFILE:
            return MaterialPageRoute(builder: (context) {
              return UserDetails(arguments['user'], arguments['commentsController']);
            });
            break;
           case Routes.BOARDED_USERS:
            return MaterialPageRoute(builder: (context) {
              return BoardedUsers(arguments['users'], arguments['deboardedUsers']);
            });
            break;
             case Routes.PICKUP_LOCATIONS:
            return MaterialPageRoute(builder: (context) {
              return PickUpLocations(arguments['user'], arguments['pickUps']);
            });
            break;
          default:
        }
      },
    );
  }
}
