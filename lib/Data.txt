1. Every user should be able to contact Admin through Phone number -- So Admin's number should be stored in database
2. Incase of Multiple Admins, Divide users to Admins
3. AdminPage should receive unique Id (maybe Company, inorder to know which database to query) from authHandler page, so that it can access the whole user object.

User Page ----------

1. UserPage should receive unique Id (maybe Company, inorder to know which database to query) from authHandler page, so that it can access the whole user object.
2. onclick of notifications icon in appBar
3. onclick of EditProfile, Navigate to profile page with user data
4. Given Static Height to each and every card --> Should change that

5. Onclick of Camera button --> Barcode scan
6. Onclick of GPS button, location of driver
7. Every upcoming trip will have a day and time stored

8. Sending ongoingTrip details to ongoingTrip card
9. Sending upcomingTrips details and required user details to upcomingTrips card
10. Display a container when trips are none

11. OnClick of location details in upcoming Trips card, Map needs to be shown
12. OnClick of Not Coming button in upcoming Trips Card, data needs to be uploaded in database

13. Onclick of emoticon and Comment, Modal and then data to be posted into that spec database
14. Emoticons data to be updated into trip database.
15. If possible, Can alter user data and not show feedback again.


16. For Attendance, 
     a. Driver will install the app
     b. on his home page, he will see an option --> Start trip for all upcoming trips.
     c. When he clicks on it, trip changes to ongoing and Driver can see the list of all users associated with that spec trip. with a right and wrong button on it.
     d. So he can click on those buttons respective to each user, depending on their attendance.

17. BoardedUsers.dart is just for practice purpose. Can add it the app too, if needed!

18. UserDetails.dart --> Is the user really gonna edit his profile or is it just gonna be static..!!!



===========================================================================================================================================

Flow of the project and interactions with database

==========================================================================

1. Login Page --> When clicks on login, Authentication is being done using email and password.

2. On a valid attempt, User Data is being queried from 'UserDataV5' collections based on the email user entered.  // If possible, make email as document of your id.

3. Now userGroupe is being checked and the respective home page is being rendered.

4. If it's Admin or Driver, Respective pages are to be rendered with required arguments being passed, which is to be discussed later.

5. If it's User, User page to be rendered with User object from the fireStore.

============================================================================

User_Home

============================================================================

1. Check whether the user have any ongoingTrip --> if No, show a dialogue box.. if Yes, pass the trip id and user id (needs to be added to boardedUsers and deboardedUsers)to Ongoing Trip page.

2. Ongoing Trip is queried from tripsDatav5 using the id provided. 

3. OngoingTrip --> Distance coming from trip geolocation
               --> call button --> number coming from trip driverContact
               --> gps button --> Disabled for now --> track the driver
               --> attendance button --> change the user data for that trip as NOT COMING
                   --> To update the attendance, using transaction, Directly updating userId to the BoardedUsers array of trip data .. if required we can use a get operation to check whether user already boarded.
                       ---> On boarding, adding user object from users map to boardedUsers map in tripsData
                   ---> On deboarding, isOngoingTrip of user needs to be changed!
                        ---> isOngoingTrip = false
                        ---> ongoingTripId = null
                        -->  tripId should be added to completedTripIds array
                        --> adding user object from users map to deboardedUser map in tripsData
4. OngoingTrip is being rendered with the help of streamBuilder subscription to userData. So after departing, userHome ongoingTrip will also be changed.

5. UpcomingTrip --> 
               --> Onclick of not coming button, tripId needs to be removed from upcomingId button
               --> Users in tripdata is maintained as an Array and required data is changed.
6. CompletedTrip --> 
               --> Onclick of feedback, need to increment the count of that feedback in feedbacks array of that trip



=================================================================================

Admin_Home

==================================================================================

1. Admin appBar need to be looked into..!!!

2. Admin Parameters      --> AdminHome, He will see all ongoingTrips, For this query tripsDatav5 based on status

3. For every trip, usersBoarded and distance needs subscription --> Hence StreamBuilder

4. For appBar and profile, he might need the userId or user object.

5. BoardedUsers page maxHeight should be looked into.


====================================================================================

Driver_Home

====================================================================================

1. Driver should see all his upcoming trips on login 

2. If he starts a trip, status of trip changes to ongoing and he will have an option to end. --> Once he ends, trip status changes to completed.

3. For his ongoing trip, --> Details to be shown are to be discussed

4. ongoingTrip card is also being rendered using streamBuilder hoping that it would help for location tracking and pickUps if needed, Can remove streamBuilder if not needed
       --> If streamBuilder is not used for fireStore subscription, some function needs to be called for getting the trip details which is coming from the id of the parent subscription. Again streamBuilder needs to be used since that's gonna return future and function should be async.
  
5. On click of start trip by driver,
         --> That specific tripId needs to be moved into ongoingTripId from upcomingTripIds and isOngoingTrip should be set to true in UserData
         --> Trip Status should be changed from upcoming to ongoing in tripData
         --> All the users data associated with that spec trip should be changed since that data is being used to render
         --> Or else, just the trip Ids array needs to be given from userData and just subscribe to that trips Data --- In this case, ongoing trip should be displayed by checking the boarding status of user

6. On click of end trip by driver,
         --> That specific tripId needs to be moved into completedTripIds, isOngoingTrip should be set to false and ongoingTripId should be null
         --> Trip Status should be changed from ongoing to completed

Important --

1. In User page, Subscription is done seperately for user in main page and no subscription to trips collection in ongoingTrip widget.. Just retreiving data from trips and setState().
2. In Driver page, Subscriptin is done for user in main page and subscription is done separately for trips collection in ongoingTrip widget.
3. In Admin page, only ongoing trips are shown in home page. Hence status field in tripDtaa is required.
4. used containerHeight in user_page for MORE/HIDE --> Working fine without containerHeight in Driver_Page.
5. When a trip status is changed to ongoing, all the users data associated with that data should be changed --> This is taken care as an action Handler in frontEnd. It would be better if this could be taken in backend.
6. Users array in trips data is considered to contain only ids and proceeded along with that. Changes need to be done! --> 'Cause can't be able to modify the map data while NOT COMING
7.





====================================================================================================

Things to be Considered

==========================================

1. Can we make id of UserDataV5 equal to emailId

2. For Ongoing Trips, currentLat and currentLon parameters are needed for distance calculation --> Using gelLocation

3. How are we supposed to get the ongoing, upcoming and completed trips.... isOngoing Trip helps!

4. How are we going to get adminContact --> From user database?

5. Each trip should have an origin and route_no.

6. There is no userType associated with the users (userType == user)
 
7. actualStartDate for every trip  or else TimeStamp instead of actualStartTime

8. Change all the trips status to UPCOMING

9. completedTripIds array should be added to user database

10. Users, boardedUsers and deboardedUsers field in tripData should be a Map of user objects with key equal to user Ids.

11. completedTripIds in userData should also be a Map

13. isBoarded field needs to be added to userData