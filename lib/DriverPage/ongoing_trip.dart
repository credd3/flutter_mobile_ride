import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../routes.dart';

class OngoingTrip extends StatefulWidget {
  final AsyncSnapshot snapshot;
  final Map<String, dynamic> user;

  const OngoingTrip(this.snapshot, this.user);

  @override
  State<StatefulWidget> createState() {
    return OngoingTripState();
  }
}

class OngoingTripState extends State<OngoingTrip> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection(Routes.TRIPSS_COLLECTION)
            .document(widget.snapshot.data['ongoingTripId'])
            .snapshots(),
        builder: (context, ongoingTripSnapshot) {
          return ongoingTripSnapshot.hasData
              ? Container(
                  decoration: ShapeDecoration(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  ),
                  constraints: BoxConstraints.tight(
                      Size(MediaQuery.of(context).size.width / 1.15, 280)),
                  child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Stack(
                        fit: StackFit.expand,
                        children: <Widget>[
                          Positioned(
                            top: -5.0,
                            right: -25.0,
                            child: FlatButton(
                              onPressed: () {
                                
                              },
                              color: Colors.red[700],
                              child: Text(
                                "SOS",
                                style: TextStyle(color: Colors.white),
                              ),
                              shape: CircleBorder(),
                            ),
                          ),
                          Wrap(
                            direction: Axis.vertical,
                            alignment: WrapAlignment.spaceAround,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            runAlignment: WrapAlignment.center,
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: Colors.green[900], width: 3.0),
                                ),
                                padding: EdgeInsets.all(18.0),
                                child: Icon(
                                  Icons.local_taxi,
                                  color: Colors.yellow[900],
                                  size: 32.0,
                                ),
                              ),
                              Text(
                                'Ongoing trip',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20.0,
                                    color: Colors.grey[700]),
                              ),
                              FlatButton(
                                color: Colors.orange[300],
                                child: Text(
                                  'Pick Up Locations',
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 16.0),
                                ),
                                onPressed: () {
                                   Navigator.of(context).pushNamed(
                                          Routes.PICKUP_LOCATIONS,
                                          arguments: {
                                            'user' : widget.user,
                                            'pickUps' : ongoingTripSnapshot.data['pickups']
                                          }); 
                                },
                              ),
                              FlatButton(
                                color: Colors.red[800],
                                child: Text(
                                  'End Trip',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16.0),
                                ),
                                onPressed: () {
                                  DocumentReference tripRef = Firestore.instance
                                    .collection(Routes.TRIPSS_COLLECTION)
                                    .document(ongoingTripSnapshot.data['id']);
                                DocumentReference userRef = Firestore.instance
                                    .collection(Routes.USERS_COLLECTION)
                                    .document(widget.snapshot.data['id']);
                                Firestore.instance
                                    .runTransaction((transaction) async {
                                  await transaction.update(userRef, {
                                    "isOngoingTrip": false,
                                    "ongoingTripId": "",
                                    "completedTripIds": FieldValue.arrayUnion(
                                       [ongoingTripSnapshot.data['id']])
                                  });                                                    // Updating the Driver's Data
                                  await transaction
                                      .update(tripRef, {"status": "completed"});
                                });
                                },
                              ),
                            ],
                          ),
                        ],
                      )))
              : CircularProgressIndicator();
        });
  }
}
