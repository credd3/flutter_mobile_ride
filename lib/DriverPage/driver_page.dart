import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import './upcoming_trip.dart';
import './ongoing_trip.dart';
import '../routes.dart';
import 'appBar.dart';

class DriverPage extends StatefulWidget {
  final Map<String, dynamic> user;

  const DriverPage(this.user);

  @override
  State<StatefulWidget> createState() {
    return DriverPageState();
  }
}

class DriverPageState extends State<DriverPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: driverAppBar(context, true, widget.user),
        body: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 60.0),
              color: Colors.grey[200],
              width: MediaQuery.of(context).size.width,
              constraints: BoxConstraints(minHeight: 512, maxHeight: 2000),
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection(Routes.USERS_COLLECTION)
                      .document(widget.user['id'])
                      .snapshots(),
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ? Wrap(
                            runSpacing: 30.0,
                            children: <Widget>[
                              snapshot.data['isOngoingTrip']
                                  ? OngoingTrip(snapshot, widget.user)
                                  : Container(
                                      decoration: ShapeDecoration(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5.0))),
                                      ),
                                      constraints: BoxConstraints.tight(Size(
                                          MediaQuery.of(context).size.width /
                                              1.15,
                                          70)),
                                      child: Center(
                                          child: Text(
                                        'No Ongoing trips at the moment!',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                            color:
                                                Theme.of(context).primaryColor),
                                      )),
                                    ),
                              snapshot.data['upcomingTripIds'].length != 0
                                  ? UpComingTrips(snapshot)
                                  : Container(
                                      decoration: ShapeDecoration(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5.0))),
                                      ),
                                      constraints: BoxConstraints.tight(Size(
                                          MediaQuery.of(context).size.width /
                                              1.15,
                                          70)),
                                      child: Center(
                                          child: Text(
                                        'No Upcoming trips at the moment!',
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                            color:
                                                Theme.of(context).primaryColor),
                                      )),
                                    ),
                            ],
                          )
                        : CircularProgressIndicator();
                  })),
        ));
  }
}
