import 'package:flutter/material.dart';
import './appBar.dart';

class PickUpLocations extends StatefulWidget {
  final Map<String, dynamic> user;
  final List<dynamic> pickUps;

  const PickUpLocations(this.user, this.pickUps);

  @override
  State<StatefulWidget> createState() {
    return PickUpLocationsState();
  }
}

class PickUpLocationsState extends State<PickUpLocations> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: driverAppBar(context, false, widget.user),
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(minHeight: 512, maxHeight: 1024),
          padding: EdgeInsets.all(10.0),
          child: widget.pickUps.length == 0 ? Text('No PickUp Locations'): ListView.separated(
            itemCount: widget.pickUps.length,
            itemBuilder: (context, index) {
              return ListTile(
                enabled: true,
                onTap: () {
                  print('tapped');
                  // Can open Maps
                },
                title: Text(widget.pickUps[index]['stopArea']),
                leading: Padding(
                  padding: EdgeInsets.only(top: 3.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.deepOrange,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      '${index + 1}',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                // trailing: Text('${users[index]['phoneNo']}'),
                // trailing: Can show distance as trailing
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          ),
        ),
      ),
    );
  }
}
