import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../routes.dart';

class UpComingTrips extends StatefulWidget {
  final AsyncSnapshot snapshot;

  const UpComingTrips(this.snapshot);

  @override
  State<StatefulWidget> createState() {
    return UpComingTripsState();
  }
}

class UpComingTripsState extends State<UpComingTrips> {
  bool moreView = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ShapeDecoration(
        color: Colors.white,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
      ),
      width: MediaQuery.of(context).size.width / 1.15,
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Wrap(
            spacing: 20.0,
            direction: Axis.vertical,
            alignment: WrapAlignment.spaceAround,
            runAlignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.orange[900], width: 3.0),
                ),
                padding: EdgeInsets.all(18.0),
                child: Icon(
                  Icons.local_taxi,
                  color: Colors.yellow[900],
                  size: 32.0,
                ),
              ),
              Text(
                'Upcoming trips',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 20.0,
                    color: Colors.grey[700]),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.5,
                child: Divider(
                  height: 1.0,
                  color: Colors.grey,
                ),
              ),
            ],
          ),
          SizedBox(height: 25.0),
          ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount:
                moreView ? widget.snapshot.data['upcomingTripIds'].length : 1,
            itemBuilder: (context, index) {
              return StreamBuilder(
                stream: Firestore.instance
                    .collection(Routes.TRIPSS_COLLECTION)
                    .document(widget.snapshot.data['upcomingTripIds'][index])
                    .snapshots(),
                builder: (context, upcomingTripSnapshot) {
                  return upcomingTripSnapshot.hasData
                      ? Center(
                          child: this.getSingleTrip(upcomingTripSnapshot.data),
                        )
                       : Center(child: CircularProgressIndicator());
                },
              );
            },
            separatorBuilder: (context, index) {
              return Icon(
                Icons.more_horiz,
                color: Colors.grey,
              );
            },
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Column(
              children: moreView
                  ? <Widget>[
                      InkWell(
                        child: Icon(Icons.arrow_drop_up),
                        onTap: () {
                          setState(() {
                            moreView = false;
                          });
                        },
                      ),
                      Text(
                        "HIDE",
                        style: TextStyle(
                            fontSize: 10.0, fontWeight: FontWeight.w300),
                      ),
                    ]
                  : <Widget>[
                      InkWell(
                        child: Icon(Icons.more_horiz),
                        onTap: () {
                          setState(() {
                            moreView = true;
                          });
                        },
                      ),
                      Text(
                        "MORE",
                        style: TextStyle(
                            fontSize: 10.0, fontWeight: FontWeight.w300),
                      ),
                    ],
            ),
          )
        ]),
      ),
    );
  }

  Widget getSingleTrip(trip) {
    return Padding(
        padding: EdgeInsets.only(
          top: 10.0,
        ),
        child: Wrap(
          direction: Axis.vertical,
          spacing: 5.0,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: <Widget>[
            Wrap(
              direction: Axis.vertical,
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 4.0,
              children: <Widget>[
                Text(
                  '${trip['actualStartDate']}',
                  style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Text(
                  '${trip['actualStartTime']}',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            FlatButton(
              disabledColor: Colors.grey[400],
              color: Colors.green[800],
              child: Text(
                'Start Trip',
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
              onPressed: !widget.snapshot.data['isOngoingTrip']
                  ? () {
                      DocumentReference tripRef = Firestore.instance
                          .collection(Routes.TRIPSS_COLLECTION)
                          .document(trip['id']);
                      DocumentReference userRef = Firestore.instance
                          .collection(Routes.USERS_COLLECTION)
                          .document(widget.snapshot.data['id']);
                      Firestore.instance.runTransaction((transaction) async {
                        await transaction.update(userRef, {
                          "isOngoingTrip": true,
                          "ongoingTripId": trip['id'],
                          "upcomingTripIds":
                              FieldValue.arrayRemove([trip['id']])
                        });                                                       // Updating the Driver's Data
                        for (var key in trip['users'].keys.toList()) {
                          DocumentReference userRef = Firestore.instance
                              .collection(Routes.USERS_COLLECTION)
                              .document(key);
                          await transaction.update(userRef, {
                            "isOngoingTrip": true,
                            "ongoingTripId": trip['id'],
                            "upcomingTripIds" : FieldValue.arrayRemove([trip['id']])
                          });
                        }                                 // Updating the data of all the users associated with the trip
                        await transaction
                            .update(tripRef, {"status": "ongoing"});
                      });
                    }                                                             // Updating the data of trip (for admin sake)
                  : null,
            ),
          ],
        ));
  }
}
