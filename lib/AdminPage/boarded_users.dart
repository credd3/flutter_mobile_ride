import 'package:flutter/material.dart';
import './appbar.dart';

class BoardedUsers extends StatefulWidget {
  final Map<dynamic ,dynamic> users;
  final Map<dynamic ,dynamic> deboardedUsers;
  const BoardedUsers(this.users, this.deboardedUsers);

  @override
  State<StatefulWidget> createState() {
    return BoardedUsersState();
  }
}

class BoardedUsersState extends State<BoardedUsers> {

  List<dynamic> users;
  Map<dynamic ,dynamic> deboardedUsers;
  
  @override
  void initState() {
    super.initState();
    setState(() {
      users = widget.users.values.toList();
      deboardedUsers = widget.deboardedUsers; 
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: adminAppBar(context, false),
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(minHeight: 512, maxHeight: 1024),
          padding: EdgeInsets.all(10.0),
          child: users.length == 0 ? Text('No Users Boarded!'): ListView.separated(
            itemCount: users.length,
            itemBuilder: (context, index) {
              return ListTile(
                enabled: true,
                onTap: () {
                  print('tapped');
                },
                title: Text(users[index]['name']),
                leading: Padding(
                  padding: EdgeInsets.only(top: 3.0),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      color: Colors.deepOrange,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      '${index + 1}',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                subtitle: deboardedUsers.containsKey(users[index]['id']) ? Text('deboarded') : null,
                trailing: Text('${users[index]['phoneNoFromRouteDB']}'),
              );
            },
            separatorBuilder: (context, index) {
              return Divider();
            },
          ),
        ),
      ),
    );
  }
}
