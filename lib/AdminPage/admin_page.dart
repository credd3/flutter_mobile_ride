import 'package:flutter/material.dart';

import './appbar.dart';
import './showRoutes.dart';
import './TrackDrivers.dart';

import 'package:geolocator/geolocator.dart';

class AdminPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AdminPageState();
  }
}

class AdminPageState extends State<AdminPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: adminAppBar(context, true),
      body: Container(
          constraints: BoxConstraints(minHeight: 512),
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 12.0),
          decoration: BoxDecoration(
              color: Color.fromRGBO(128, 128, 128, 1.0),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                // Add one stop for each color. Stops should increase from 0 to 1
                stops: [0.2, 0.5, 0.7, 0.9],
                colors: [
                  // Colors are easy thanks to Flutter's Colors class.
                  Color.fromRGBO(128, 128, 128, 0.1),
                  Color.fromRGBO(128, 128, 128, 0.3),
                  Color.fromRGBO(128, 128, 128, 0.5),
                  Color.fromRGBO(128, 128, 128, 0.7),
                ],
              )),
          child: AdminBody(),
        ),
    );
  }
}

class AdminBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0)),
          )),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 14.0),
      // child: Column(
      //  // mainAxisSize: MainAxisSize.min,
      //   children: <Widget>[
          // Card(
          //     margin: EdgeInsets.only(bottom: 30.0),
          //     color: Color.fromRGBO(128, 128, 128, 0.5),
          //     child: Container(
          //       constraints: BoxConstraints.expand(height: 84),
          //       padding: EdgeInsets.only(top: 10.0),
          //       child: Wrap(
          //         direction: Axis.vertical,
          //         alignment: WrapAlignment.spaceBetween,
          //         crossAxisAlignment: WrapCrossAlignment.center,
          //         runAlignment: WrapAlignment.center,
          //         children: <Widget>[
          //           Text(
          //             "Ongoing Trips",
          //             style: TextStyle(
          //               fontSize: 22.0,
          //             ),
          //           ),
          //           FlatButton(
          //             child: Text(
          //               "TRACK ALL",
          //               style: TextStyle(
          //                   fontSize: 16.0,
          //                   color: Colors.pink,
          //                   letterSpacing: 1.0),
          //             ),
          //             onPressed: () async {
          //               Position pos = await Geolocator().getCurrentPosition(
          //                   desiredAccuracy: LocationAccuracy.high);
          //               Navigator.of(context).push(MaterialPageRoute(
          //                   builder: (context) => TrackDrivers(pos)));
          //             },
          //           ),
          //         ],
          //       ),
          //     )),
         child: ShowRoutes()
      //  ],
      // ),
    );
  }
}
