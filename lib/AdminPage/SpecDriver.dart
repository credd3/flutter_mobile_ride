import 'package:flutter/material.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:geolocator/geolocator.dart';

class SpecDriver extends StatefulWidget {
  final Position pos;
  final Map<String, dynamic> route;

  SpecDriver(this.pos, this.route);

  @override
  State<StatefulWidget> createState() {
    return SpecDriverState();
  }
}

class SpecDriverState extends State<SpecDriver> {
  GoogleMapController mapController;
  Map<MarkerId, Marker> markers =
      <MarkerId, Marker>{}; // Stores all the Markers
  Map<PolylineId, Polyline> polyLines = <PolylineId,
      Polyline>{}; // Stores all the points for the route to be drawn
  Map<String, dynamic> route; // Stores data associated with the specific route

  @override
  void initState() {
    super.initState();
    setState(() {
      route = widget.route;
    });
  }

  Future<Marker> _driverLocation() async {
    double distanceInMeters, distanceInKms;
    distanceInMeters = await Geolocator().distanceBetween(
        widget.pos.latitude, widget.pos.longitude, route['lat'], route['lon']);
    distanceInKms = distanceInMeters / 1000.0;
    return Marker(
      position: LatLng(route['lat'], route['lon']),
      markerId: MarkerId(route['route_no']),
      infoWindow: InfoWindow(
          title: 'Driver Name',
          snippet: '$distanceInKms kms away from your location'),
    );
  }

  Future<Marker> _destinationMarker() async {
    return Marker(
      position: LatLng(widget.pos.latitude, widget.pos.longitude),
      markerId: MarkerId('destination'),
      infoWindow: InfoWindow(
        title: 'Destination',
      ),
    );
  }

  Map<String, dynamic> _pickUp() {
    Map<MarkerId, Marker> pickUPMarkers = <MarkerId, Marker>{};
    final List<LatLng> pickUpPoints = <LatLng>[];
    route['pickUpPoints'].forEach((pickUpPoint) {
      pickUPMarkers[MarkerId(pickUpPoint['passenger_no'])] = Marker(
        position: LatLng(pickUpPoint['lat'], pickUpPoint['lon']),
        markerId: MarkerId(pickUpPoint['passenger_no']),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
      );
      pickUpPoints.add(LatLng(pickUpPoint['lat'], pickUpPoint['lon']));
    });
    return {
      "pickUpMarkers" : pickUPMarkers,
      "pickUpPoints" : pickUpPoints
    };
  }        // Locating Markers and points at pickUp Locations

  void _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
    final Map<String, dynamic> pickUp = _pickUp();
    final List<LatLng> points = <LatLng> [];

    points.add(LatLng(route['lat'], route['lon']));                                      // Driver Location
    points.addAll(pickUp['pickUpPoints']);                                               // PickUp Locations
    points.add( LatLng(widget.pos.latitude, widget.pos.longitude));                      // Destination Location
    
    final Polyline polyline = Polyline(
      polylineId: PolylineId('1'),
      consumeTapEvents: true,
      color: Colors.red,
      width: 5,
      points: points,                                                                    // Setting up the polyLine points
      patterns: <PatternItem>[
      PatternItem.dash(20.0),
      PatternItem.gap(3.0),
      PatternItem.dot,
      PatternItem.gap(3.0)
    ],
    geodesic: true,
    // endCap: Cap.buttCap,
    // startCap: Cap.buttCap,
    // jointType: JointType.mitered
    );
    
    Marker driverLocation = await _driverLocation();
    Marker destinationMarker = await _destinationMarker();
    
    setState((){
      
      markers[MarkerId(route['route_no'])] = driverLocation;                             // Setting Up Marker at Driver Location
      markers[MarkerId('destination')] = destinationMarker;                              // Setting Up Marker at Admin Location (Organisation's Location)
      markers.addAll(pickUp['pickUpMarkers']);                                           // Setting Up Markers at PickUpLocations
      
      polyLines[PolylineId('1')] = polyline;                                              // Setting Up polyLine

    });
  }

  void _onCameraMove(CameraPosition position) {
    mapController.animateCamera(CameraUpdate.newLatLng(
      LatLng(position.target.latitude, position.target.longitude),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: LatLng(widget.pos.latitude, widget.pos.longitude),
            zoom: 15.0,
          ),
          markers: Set<Marker>.of(markers.values),
          polylines: Set<Polyline>.of(polyLines.values),
          myLocationEnabled: true,
          onCameraMove: _onCameraMove,
        ),
        Positioned(
          child: FlatButton(
            child: Container(
              padding: EdgeInsets.all(12.0),
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 20,
              ),
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.black),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          top: 30,
          left: 5,
        ),
      ],
    );
  }
}

// route should have driver's location as lat and lon and array of pickUp points --> again lat and lon (Markers)
