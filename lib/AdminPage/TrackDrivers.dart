import 'package:flutter/material.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:geolocator/geolocator.dart';

class TrackDrivers extends StatefulWidget {
  final Position pos;
  TrackDrivers(this.pos);

  @override
  State<StatefulWidget> createState() {
    return TrackDriversState();
  }
}

class TrackDriversState extends State<TrackDrivers> {
  GoogleMapController mapController;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  List<dynamic> _routes;

  @override
  void initState() {
    super.initState();
    this.getJsonData();
  }

  Future<dynamic> getJsonData() async {
    var response = await http.get(
        Uri.encodeFull('http://10.150.93.7:3000/routes'),
        headers: {"Accept": "application/json"});

    setState(() {
      var converDataToJson = jsonDecode(response.body);
      _routes = converDataToJson;
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      mapController = controller;
      double distanceInMeters, distanceInKms;
            _routes.forEach((route) async =>  {
            distanceInMeters = await Geolocator().distanceBetween(widget.pos.latitude, widget.pos.longitude, route['lat'], route['lon'] ),
            distanceInKms = distanceInMeters/1000.0,
            markers[MarkerId(route['route_no'])] = Marker(
                position: LatLng(route['lat'], route['lon']),
                markerId: MarkerId(route['route_no']),
                infoWindow: InfoWindow(title: 'Driver Name',snippet: '$distanceInKms kms away from your location'),
                )
          });
    });
  }

  void _onCameraMove(CameraPosition position) {
    mapController.animateCamera(CameraUpdate.newLatLng(
      LatLng(position.target.latitude, position.target.longitude),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: LatLng(widget.pos.latitude, widget.pos.longitude),
            zoom: 14.0,
          ),
          markers: Set<Marker>.of(markers.values),
          myLocationEnabled: true,
          onCameraMove: _onCameraMove,
        ),
        Positioned(
          child: FlatButton(
            child: Container(
              padding: EdgeInsets.all(12.0),
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 20,
              ),
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.black),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          top: 30,
          left: 5,
        ),
      ],
    );
  }
}
