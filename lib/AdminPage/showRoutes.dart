import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:geolocator/geolocator.dart';

import './SpecDriver.dart';
import '../routes.dart';
import './boarded_users.dart';

class ShowRoutes extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ShowRoutesState();
  }
}

class ShowRoutesState extends State<ShowRoutes> {
  @override
  void initState() {
    super.initState();
  }

  Future<dynamic> getDistance(Map<String, dynamic> trip) async {
    double distanceInMeters, distanceInKms;
    distanceInMeters = await Geolocator().distanceBetween(
        trip['destination'].latitude,
        trip['destination'].longitude,
        trip['geoLocation'].latitude,
        trip['geoLocation'].longitude);
    distanceInKms = distanceInMeters / 1000.0;
    return distanceInKms.toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Firestore.instance
          .collection(Routes.TRIPSS_COLLECTION)
          .where('status', isEqualTo: 'ongoing')
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<DocumentSnapshot> trips = snapshot.data.documents;
          return ListView.builder(
            shrinkWrap: true,
            itemCount: trips.length,
            itemBuilder: snapshot.data != null
                ? (BuildContext context, int index) {
                    return Wrap(
                        direction: Axis.vertical,
                        spacing: 0.0,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 1.2,
                            child: Card(
                              color: Colors.black,
                              child: Padding(
                                padding: EdgeInsets.all(7.0),
                                child: Text(
                                  'Driver Name:  ${trips[index]["driverName"]}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width/1.2,
                            child: Card(
                            margin: EdgeInsets.only(bottom: 20.0),
                            color: Colors.grey[200],
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0)),
                            child: Padding(
                              padding: EdgeInsets.only(
                                top: 7,
                                bottom: 7,
                                left: 10.0,
                              ),
                              child: Wrap(
                                direction: Axis.horizontal,
                                alignment: WrapAlignment.spaceAround,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                runAlignment: WrapAlignment.spaceAround,
                                spacing: MediaQuery.of(context).size.width / 10,
                                children: <Widget>[
                                  Wrap(
                                    direction: Axis.vertical,
                                    crossAxisAlignment:
                                        WrapCrossAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        '${index + 1}', // Route name should come from database
                                        style: TextStyle(
                                            fontSize: 32.0,
                                            color: Colors.orange),
                                      ),
                                      Text(
                                        'ROUTE',
                                        style: TextStyle(fontSize: 10.0),
                                      ),
                                    ],
                                  ),
                                  InkWell(
                                    child: Wrap(
                                      direction: Axis.vertical,
                                      crossAxisAlignment:
                                          WrapCrossAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          '${trips[index]["boardedUsers"].length - trips[index]["deboardedUsers"].length}',
                                          style: TextStyle(
                                            fontSize: 28.0,
                                          ),
                                        ),
                                        Text(
                                          'User/s',
                                          style: TextStyle(fontSize: 10.0),
                                        ),
                                        Text(
                                          'On Board',
                                          style: TextStyle(fontSize: 10.0),
                                        ),
                                      ],
                                    ),
                                    onTap: () {
                                      Navigator.of(context).pushNamed(
                                          Routes.BOARDED_USERS,
                                          arguments: {
                                            'users': trips[index]
                                                ['boardedUsers'],
                                            'deboardedUsers' : trips[index]['deboardedUsers']
                                          });
                                    },
                                  ),
                                  Wrap(
                                    direction: Axis.vertical,
                                    crossAxisAlignment:
                                        WrapCrossAlignment.center,
                                    children: <Widget>[
                                      StreamBuilder(
                                        stream: getDistance(trips[index].data)
                                            .asStream(),
                                        builder: (context, distance) {
                                          return distance.hasData
                                              ? Text(
                                                  '${distance.data}',
                                                  style: TextStyle(
                                                      fontSize: 24.0,
                                                      color: Colors.green),
                                                )
                                              : CircularProgressIndicator();
                                        },
                                      ),
                                      Text(
                                        'kms away',
                                        style: TextStyle(fontSize: 10.0),
                                      ),
                                    ],
                                  ),
                                  InkWell(
                                      child: Icon(
                                        Icons.navigation,
                                        color: Colors.deepOrange,
                                        size: 32.0,
                                      ),
                                      onTap: () async {
                                        // Get Location from database and keep sending it to SpecDriver page
                                        // Position pos = await Geolocator()
                                        //     .getCurrentPosition(
                                        //         desiredAccuracy:
                                        //             LocationAccuracy.high);
                                        // Navigator.of(context).push(
                                        //     new MaterialPageRoute(
                                        //         builder: (context) =>
                                        //             SpecDriver(
                                        //                 pos, _routes[index])));
                                      }),
                                ],
                              ),
                            ),
                          ),
                          )
                        ]);
                  }
                : (BuildContext context, int index) {
                    return Text('Loading');
                  },
          );
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }
}
// If wraps for more items, set shrinkWrap to false and wrap our ListView widget in Expanded()

// Distance is giving null sometimes, Don't know the exact reason .... So keeping a null check
