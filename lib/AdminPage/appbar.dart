import 'package:flutter/material.dart';

import '../routes.dart';

Widget adminAppBar(context, isHomePage) {
  return  AppBar(
          title: Text(
            "SapeRides",
          ),
          leading: Padding(
            padding: EdgeInsets.all(10.0),
            child: Container(
              padding: EdgeInsets.all(3.0),
              child: InkWell(
                child: Image.asset('assets/sapeRidesLogo.png'),
                onTap: () {
                  if(!isHomePage) Navigator.of(context).pushNamedAndRemoveUntil(Routes.ADMIN_HOME, (Route route) => route.settings.name == Routes.ADMIN_HOME);
                },
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
              ),
            ),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.notifications),
              onPressed: () {
                // Navigate to the notifications page (or) show it as a popUpMenu
              },
            ),
            PopupMenuButton(
                icon: Icon(Icons.account_circle),
                itemBuilder: (context) => [
                      PopupMenuItem(
                        value: 1,
                        child: Text('Admin', style: TextStyle(color: Colors.grey[900]),),
                        enabled: false,
                      ),
                      PopupMenuItem(
                        value: 2,
                        child: Text("Edit Profile"),
                      ),
                      PopupMenuItem(
                        value: 3,
                        child: Text("LogOut"),
                      ),
                    ],
                offset: Offset(0, 100),
                onSelected: (value) {
                  switch (value) {
                    case 3:
                      Navigator.of(context).pushNamedAndRemoveUntil(Routes.DEFAULT_ROUTE, (Route route) => route == null);
                      break;
                    case 2:
                    // Navigate to profile page
                    default:
                    // Do Nothing
                  }
                }),
          ]);
}