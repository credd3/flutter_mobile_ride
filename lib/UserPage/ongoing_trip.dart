import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import '../routes.dart';
import './popup.dart';

class OnGoingTrip extends StatefulWidget {
  final String ongoingTripId;
  final String userId;

  const OnGoingTrip(
    this.userId,
    this.ongoingTripId,
  );

  @override
  State<StatefulWidget> createState() {
    return OnGoingTripState();
  }
}

class OnGoingTripState extends State<OnGoingTrip> {
  double distance;
  bool isBoarded = false; // This needs to be coming from database
  DocumentReference tripRef;
  DocumentReference userRef;
  // Map<String, dynamic> ongoingTrip = {};

  @override
  void initState() {
    super.initState();
    setState(() {
      tripRef = Firestore.instance
          .collection(Routes.TRIPSS_COLLECTION)
          .document(widget.ongoingTripId);
      userRef = Firestore.instance
          .collection(Routes.USERS_COLLECTION)
          .document(widget.userId);
      userRef.get().then((onValue){
        setState(() {
           isBoarded = onValue.data['isBoarded'];
        });
      });
    });
    // this.getTripData().then((onValue) {
    //   setDistance();
    // });
  }

  // getTripData() {
  //   return tripRef.get().then((snapshot) {
  //     setState(() {
  //       ongoingTrip = snapshot.data;
  //     });
  //   });
  // }

  Future<dynamic> setDistance(ongoingTrip) async {
    Position pos = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    double distanceInMeters;
    distanceInMeters = await Geolocator().distanceBetween(
        pos.latitude,
        pos.longitude,
        ongoingTrip['geoLocation'].latitude,
        ongoingTrip['geoLocation'].longitude);
       
    return (distanceInMeters / 1000.0).toStringAsFixed(2);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
        ),
        constraints: BoxConstraints.tight(
            Size(MediaQuery.of(context).size.width / 1.15, 280)),
        child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Positioned(
                  top: -5.0,
                  right: -25.0,
                  child: FlatButton(
                    onPressed: () {
                      /* trigger a message and call to Admin or personal contact */
                    },
                    color: Colors.red[700],
                    child: Text(
                      "SOS",
                      style: TextStyle(color: Colors.white),
                    ),
                    shape: CircleBorder(),
                  ),
                ),
                StreamBuilder(
                  stream: Firestore.instance
                      .collection(Routes.TRIPSS_COLLECTION)
                      .document(widget.ongoingTripId)
                      .snapshots(),
                  builder: (context, snapshot) {
                    return snapshot.hasData
                        ? Wrap(
                            direction: Axis.vertical,
                            alignment: WrapAlignment.spaceAround,
                            runAlignment: WrapAlignment.center,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: Colors.green[900], width: 3.0),
                                ),
                                padding: EdgeInsets.all(18.0),
                                child: Icon(
                                  Icons.local_taxi,
                                  color: Colors.yellow[900],
                                  size: 32.0,
                                ),
                              ),
                              Text(
                                'Ongoing trip',
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: 20.0,
                                    color: Colors.grey[700]),
                              ),
                              StreamBuilder(
                                stream: setDistance(snapshot.data).asStream(),
                                builder: (context, distanceSnapshot) {
                                  return distanceSnapshot.hasData
                                     ? Text(
                                          'Your Cab is ${distanceSnapshot.data} kms Away',
                                          style: TextStyle(
                                              fontStyle: FontStyle.italic,
                                              color: Colors.green[700],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20.0),
                                        )
                                      : SizedBox(height: 10.0, child: CircularProgressIndicator(),);
                                },
                              ),
                              Wrap(
                                direction: Axis.horizontal,
                                alignment: WrapAlignment.spaceBetween,
                                spacing: MediaQuery.of(context).size.width / 12,
                                children: <Widget>[
                                  GestureDetector(
                                    child: Icon(
                                      Icons.call,
                                      size: 24.0,
                                    ),
                                    onTap: () {
                                      UrlLauncher.launch(
                                          'tel://${snapshot.data['driverContact']}');
                                    },
                                  ),
                                  GestureDetector(
                                    child: Icon(
                                      Icons.gps_fixed,
                                      size: 24.0,
                                      color: Colors.grey,
                                    ),
                                    onTap: () {
                                      // ShowMap
                                    },
                                  ),
                                  GestureDetector(
                                      child: Icon(
                                        Icons.camera,
                                        size: 24.0,
                                      ),
                                      onTap: () {
                                        PopUp.showAttendanceButton(
                                                context, isBoarded)
                                            .then((result) {
                                          Firestore.instance.runTransaction(
                                              (transaction) async {
                                          DocumentSnapshot snapshot = await tripRef.get();
                                            if (result == "boarded") {
                                              await transaction
                                                  .update(tripRef, {
                                                "boardedUsers.${widget.userId}": snapshot.data['users'][widget.userId]
                                              }).then((onValue) {
                                                setState(() {
                                                  isBoarded = true;
                                                });
                                              });
                                              await transaction.update(userRef, {
                                                'isBoarded' : true
                                              });
                                            } else if (result == "departed") {
                                              await transaction
                                                  .update(tripRef, {
                                               "deboardedUsers.${widget.userId}": snapshot.data['users'][widget.userId]
                                              }).then((onValue) async {
                                                await transaction
                                                    .update(userRef, {
                                                      "isBoarded" : false,
                                                  "isOngoingTrip": false,
                                                  "ongoingTripId": "",
                                                  "completedTripIds.${widget.ongoingTripId}.id":
                                                      widget.ongoingTripId
                                                });
                                              });
                                            }
                                          });
                                        });
                                      }),
                                ],
                              ),
                              GestureDetector(
                                child: Icon(
                                  Icons.report_off,
                                  size: 32.0,
                                  color: Colors.red[900],
                                ),
                                onTap: () {},
                              ),
                            ],
                          )
                        : Center(child: CircularProgressIndicator());
                  },
                )
              ],
            )));
  }
}
