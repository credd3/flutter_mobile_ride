import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import '../routes.dart';

import './ongoing_trip.dart';
import './upcoming_trip.dart';
import './completed_trip.dart';
import './appbar.dart';

class UserPage extends StatefulWidget {
  final Map<String, dynamic> user;

  const UserPage(this.user);

  @override
  State<StatefulWidget> createState() {
    return UserPageState();
  }
}

class UserPageState extends State<UserPage> {
  Map<String, dynamic> user; // All Details regarding to user
  TextEditingController commentsController = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      user = widget.user;
    });
  }

 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: userAppBar(context, user, commentsController, true),
        body: SingleChildScrollView(
            child: Container(
          color: Colors.grey[200],
          constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height - 100.0,
              minWidth:
                  MediaQuery.of(context).size.width), // Reducing AppBar Height
          padding: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width / 15,
              vertical: MediaQuery.of(context).size.width / 10),
          child: StreamBuilder(
            stream: Firestore.instance
                .collection(Routes.USERS_COLLECTION)
                .document(user['id'])
                .snapshots(),
            builder: (context, snapshot) {
             print(snapshot.data);
              return snapshot.hasData
                  ? Wrap(
                      direction: Axis.vertical,
                      spacing: 30.0,
                      children: <Widget>[
                          snapshot.data['isOngoingTrip']
                              ? OnGoingTrip(snapshot.data['id'],
                                  snapshot.data['ongoingTripId'])
                              : Container(
                                  decoration: ShapeDecoration(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0))),
                                  ),
                                  constraints: BoxConstraints.tight(Size(
                                      MediaQuery.of(context).size.width / 1.15,
                                      70)),
                                  child: Center(
                                      child: Text(
                                    'No Ongoing trips at the moment!',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                        color: Theme.of(context).primaryColor),
                                  )),
                                ),
                          snapshot.data['upcomingTripIds'].length != 0
                              ? UpComingTrips(snapshot, user)
                              : Container(
                                  decoration: ShapeDecoration(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0))),
                                  ),
                                  constraints: BoxConstraints.tight(Size(
                                      MediaQuery.of(context).size.width / 1.15,
                                      70)),
                                  child: Center(
                                      child: Text(
                                    'No upcoming trips at the moment!',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                        color: Theme.of(context).primaryColor),
                                  )),
                                ),
                          snapshot.data['completedTripIds'] != null && snapshot.data['completedTripIds'].length != 0
                              ? CompletedTrips(
                                  snapshot, user, commentsController)
                              : Container(
                                  decoration: ShapeDecoration(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0))),
                                  ),
                                  constraints: BoxConstraints.tight(Size(
                                      MediaQuery.of(context).size.width / 1.15,
                                      70)),
                                  child: Center(
                                      child: Text(
                                    'No Completed trips at the moment!',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold,
                                        color: Theme.of(context).primaryColor),
                                  )),
                                ),
                        ])
                  : CircularProgressIndicator();
            },
          ),
        ))
        //   --> use one of the childs for verification purpose
        );
  }
}
