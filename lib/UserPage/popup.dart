import 'package:flutter/material.dart';

class PopUp {
  static showEmoticons(context) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            child: Stack(
              alignment: Alignment.center,
              fit: StackFit.loose,
              children: <Widget>[
                Positioned(
                  top: 10.0,
                  right: 12.0,
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.red[700]),
                      shape: BoxShape.circle,
                    ),
                    child: GestureDetector(
                      child: Icon(
                        Icons.close,
                        size: 17.0,
                        color: Colors.red[700],
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 30.0, top: 35.0),
                  height: 165.0,
                  child: Wrap(
                    direction: Axis.vertical,
                    alignment: WrapAlignment.spaceBetween,
                    runAlignment: WrapAlignment.center,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: <Widget>[
                      Text(
                        'Give Us Your Feedback',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      Wrap(
                        direction: Axis.horizontal,
                        spacing: 20.0,
                        children: <Widget>[
                          Wrap(
                            direction: Axis.vertical,
                            spacing: 5.0,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                child: Text(
                                  '😁',
                                  style: TextStyle(fontSize: 24.0),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop('excited');
                                },
                              ),
                              Text(
                                'Excited',
                                style: TextStyle(fontSize: 12.0),
                              )
                            ],
                          ),
                          Wrap(
                            direction: Axis.vertical,
                            spacing: 5.0,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                child: Text(
                                  '🙂',
                                  style: TextStyle(fontSize: 24.0),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop('happy');
                                },
                              ),
                              Text(
                                'Happy',
                                style: TextStyle(fontSize: 12.0),
                              )
                            ],
                          ),
                          Wrap(
                            direction: Axis.vertical,
                            spacing: 5.0,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                child: Text(
                                  '😐',
                                  style: TextStyle(fontSize: 24.0),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop('neutral');
                                },
                              ),
                              Text(
                                'Neutral',
                                style: TextStyle(fontSize: 12.0),
                              )
                            ],
                          ),
                          Wrap(
                            direction: Axis.vertical,
                            spacing: 5.0,
                            crossAxisAlignment: WrapCrossAlignment.center,
                            children: <Widget>[
                              GestureDetector(
                                child: Text(
                                  '☹️',
                                  style: TextStyle(fontSize: 24.0),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop('bad');
                                },
                              ),
                              Text(
                                'Bad',
                                style: TextStyle(fontSize: 12.0),
                              )
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ));
      },
    );
  }

  static showCommentBox(context, controller) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            child: Stack(
              fit: StackFit.loose,
              children: <Widget>[
                Positioned(
                  top: 10.0,
                  right: 12.0,
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.red[700]),
                      shape: BoxShape.circle,
                    ),
                    child: GestureDetector(
                      child: Icon(
                        Icons.close,
                        size: 17.0,
                        color: Colors.red[700],
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 0.0, top: 35.0, left: 20.0),
                  height: 220.0,
                  child: Wrap(
                    direction: Axis.vertical,
                    spacing: 15.0,
                    children: <Widget>[
                      Text(
                        'Comment your views:',
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        constraints: BoxConstraints.tight(Size(230.0, 80.0)),
                        child: TextField(
                          minLines: 2,
                          maxLines: 5,
                          decoration: InputDecoration(
                            labelText: "Enter your comments",
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              borderSide: BorderSide(),
                            ),
                          ),
                          controller: controller,
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 5.0,
                  right: 10.0,
                  child: FlatButton(
                    child: Text(
                      'Submit',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          letterSpacing: 1.3),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop(controller.text);
                      controller.clear();
                    },
                    color: Colors.teal,
                  ),
                )
              ],
            ));
      },
    );
  }

  static showAttendanceButton(context, isBoarded) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            child: Stack(
              children: <Widget>[
               Positioned(
                  top: 10.0,
                  right: 12.0,
                  child: Container(
                    padding: EdgeInsets.all(1.0),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.red[700]),
                      shape: BoxShape.circle,
                    ),
                    child: GestureDetector(
                      child: Icon(
                        Icons.close,
                        size: 17.0,
                        color: Colors.red[700],
                      ),
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ),
               Padding(padding: EdgeInsets.symmetric(horizontal: 70.0, vertical: 50.0),
               child: RaisedButton(
                    elevation: 10.0,
                    padding: EdgeInsets.all(40.0),
                    onPressed: isBoarded ? () {
                        Navigator.of(context).pop('departed');
                    } : (){
                       Navigator.of(context).pop('boarded');
                    },
                    shape: CircleBorder(),
                    color: isBoarded ? Colors.red : Colors.green,
                    child: Text(isBoarded ? 'Depart' : 'Board', style: TextStyle(fontSize: 20.0, color: Colors.white,), textAlign: TextAlign.center,),
                  ),
               )
              ],
            ));
      },
    );
  }
}
