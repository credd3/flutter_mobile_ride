import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../routes.dart';

class UpComingTrips extends StatefulWidget {
  final AsyncSnapshot snapshot;
  final Map<String, dynamic> user;
  const UpComingTrips(this.snapshot, this.user);

  @override
  State<StatefulWidget> createState() {
    return UpComingTripsState();
  }
}

class UpComingTripsState extends State<UpComingTrips> {
  bool moreView = false;
  DocumentReference userRef;
  CollectionReference tripsRef;

  @override
  void initState() {
    super.initState();
    print( widget.snapshot.data['upcomingTripIds']);
    setState(() {
      userRef = Firestore.instance.collection(Routes.USERS_COLLECTION).document(widget.user['id']);
      tripsRef = Firestore.instance.collection(Routes.TRIPSS_COLLECTION);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
        ),
        width: MediaQuery.of(context).size.width/1.15,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Wrap(
                spacing: 20.0,
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceAround,
                runAlignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.orange[900], width: 3.0),
                    ),
                    padding: EdgeInsets.all(18.0),
                    child: Icon(
                      Icons.local_taxi,
                      color: Colors.yellow[900],
                      size: 32.0,
                    ),
                  ),
                  Text(
                    'Upcoming trips',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 20.0,
                        color: Colors.grey[700]),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.5,
                    child: Divider(
                      height: 1.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 25.0),
               ListView.separated(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount:
                moreView ? widget.snapshot.data['upcomingTripIds'].length : 1,
            itemBuilder: (context, index) {
              return StreamBuilder(
                stream: Firestore.instance
                    .collection(Routes.TRIPSS_COLLECTION)
                    .document(widget.snapshot.data['upcomingTripIds'][index])
                    .snapshots(),
                builder: (context, upcomingTripSnapshot) {
                  return upcomingTripSnapshot.hasData
                      ? Center(
                          child: this.getSingleTrip(upcomingTripSnapshot.data),
                        )
                       : Center(child: CircularProgressIndicator());
                },
              );
            },
            separatorBuilder: (context, index) {
              return Icon(
                Icons.more_horiz,
                color: Colors.grey,
              );
            },
          ),
              Align(
                alignment: Alignment.bottomRight,
                child: Column(
                  children: moreView
                      ? <Widget>[
                          InkWell(
                            child: Icon(Icons.arrow_drop_up),
                            onTap: () {
                              setState(() {
                                moreView = false;
                              });
                            },
                          ),
                          Text(
                            "HIDE",
                            style: TextStyle(
                                fontSize: 10.0, fontWeight: FontWeight.w300),
                          ),
                        ]
                      : <Widget>[
                          InkWell(
                            child: Icon(Icons.more_horiz),
                            onTap: () {
                              setState(() {
                                moreView = true;
                              });
                            },
                          ),
                          Text(
                            "MORE",
                            style: TextStyle(
                                fontSize: 10.0, fontWeight: FontWeight.w300),
                          ),
                        ],
                ),
              ), // Extra Wrap for 2 rows related to a spec. trip
            ],
          ),
        ));
  }

  Widget getSingleTrip(trip) {
    return Padding(
      padding: EdgeInsets.only(bottom: 7.0, top: 5.0),
      child: Wrap(
      direction: Axis.vertical,
      spacing: 20.0,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: <Widget>[
        Wrap(
          direction: Axis.horizontal,
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 30.0,
          children: <Widget>[
            Wrap(
              direction: Axis.vertical,
              crossAxisAlignment: WrapCrossAlignment.center,
              spacing: 4.0,
              children: <Widget>[
                Text(
                  '${trip['actualStartDate']}',
                  style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Text(
                  '${trip['actualStartTime']}',
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            GestureDetector(
              child: Wrap(
                direction: Axis.vertical,
                crossAxisAlignment: WrapCrossAlignment.center,
                spacing: 4.0,
                children: <Widget>[
                  Icon(
                    Icons.place,
                    size: 20.0,
                    color: Colors.pink,
                  ),
                  Text(
                    '${widget.user['pickupLocation']['stopArea']}',
                    style: TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.normal,
                        color: Colors.green),
                  ),
                ],
              ),
              onTap: () {},
            )
          ],
        ),
        GestureDetector(
          child: Icon(
            Icons.report_off,
            size: 32.0,
            color: Colors.red[900],
          ),
          onTap: () {
            print(trip['id']);
            print(widget.user['id']);
            Firestore.instance.runTransaction((transaction) async{
              await transaction.update(userRef, {
                "upcomingTripIds" : FieldValue.arrayRemove([trip['id']])
              });
              await transaction.update(tripsRef.document(trip['id']), {
                "users.000000.isComing" : false
              });
            });
          },
        ),
      ],
    ),
    );
  }
}
