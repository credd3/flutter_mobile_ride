import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../routes.dart';
import './popup.dart';

class CompletedTrips extends StatefulWidget {
  final AsyncSnapshot snapshot;
  final Map<String, dynamic> user;
  final TextEditingController controller;

  const CompletedTrips(this.snapshot, this.user, this.controller);

  @override
  State<StatefulWidget> createState() {
    return CompletedTripsState();
  }
}

class CompletedTripsState extends State<CompletedTrips> {
  bool moreView = false;
  DocumentReference userRef;
  @override
  void initState() {
    super.initState();
    print('from inistatte');
    print(widget.user.keys.toList());
    setState(() {
      userRef = Firestore.instance
          .collection(Routes.USERS_COLLECTION)
          .document(widget.user['id']);
    });
  }

  Widget getSingleTrip(trip) {
    return Padding(
      padding: EdgeInsets.only(bottom: 7.0, top: 5.0),
      child: Wrap(
        direction: Axis.vertical,
        crossAxisAlignment: WrapCrossAlignment.center,
        spacing: 12.0,
        children: <Widget>[
          Wrap(
            direction: Axis.vertical,
            crossAxisAlignment: WrapCrossAlignment.center,
            spacing: 4.0,
            children: <Widget>[
              Text(
                "${trip['actualStartDate']}",
                style: TextStyle(
                  fontSize: 12.0,
                  fontWeight: FontWeight.normal,
                ),
              ),
              Text(
                "${trip['actualStartTime']}",
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          StreamBuilder(
            stream: userRef.snapshots(),
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? Wrap(
                      spacing: 25.0,
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.greenAccent),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(3.0)),
                            ),
                            child: GestureDetector(
                              child: Icon(
                                Icons.mood,
                                size: 24.0,
                                color: !snapshot.data['completedTripIds'][trip['id']].containsKey('feedbackSubmitted') ? Colors.deepOrange : Colors.grey,
                              ),
                              onTap: !snapshot.data['completedTripIds'][trip['id']].containsKey('feedbackSubmitted') ? () async {
                                PopUp.showEmoticons(context).then((feedback) {
                                  print(feedback);
                                  if (feedback != null) {
                                    Firestore.instance
                                        .runTransaction((transaction) async {
                                      DocumentReference tripRef = Firestore
                                          .instance
                                          .collection(Routes.TRIPSS_COLLECTION)
                                          .document(trip['id']);
                                      DocumentSnapshot snapshot =
                                          await transaction.get(tripRef);
                                      await transaction.update(tripRef, {
                                        "feedbacks.$feedback": snapshot
                                                .data['feedbacks'][feedback] +
                                            1,
                                      });
                                      await transaction.update(userRef, {
                                        "completedTripIds.${trip['id']}.feedbackSubmitted" : true
                                      });
                                    });
                                  }
                                });
                              } : null,
                            )),
                        Container(
                            padding: EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.greenAccent),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(3.0)),
                            ),
                            child: GestureDetector(
                              child: Icon(
                                Icons.mode_comment,
                                size: 24.0,
                                color:  !snapshot.data['completedTripIds'][trip['id']].containsKey('commentSubmitted') ? Colors.deepOrange : Colors.grey,
                              ),
                              onTap: !snapshot.data['completedTripIds'][trip['id']].containsKey('commentSubmitted')  ? () {
                                PopUp.showCommentBox(context, widget.controller)
                                    .then((comment) {
                                  if (comment != null) {
                                    Firestore.instance
                                        .runTransaction((transaction) async {
                                      DocumentReference tripRef = Firestore
                                          .instance
                                          .collection(Routes.TRIPSS_COLLECTION)
                                          .document(trip['id']);
                                      await transaction.update(tripRef, {
                                        "feedbacks.comments":
                                            FieldValue.arrayUnion([comment])
                                      });
                                       await transaction.update(userRef, {
                                        "completedTripIds.${trip['id']}.commentSubmitted" : true
                                      });
                                    });
                                  }
                                });
                              } : null,
                            )),
                      ],
                    )
                  : Center(
                      child: CircularProgressIndicator(),
                    );
            },
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: ShapeDecoration(
          color: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
        ),
        width: MediaQuery.of(context).size.width / 1.15,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Wrap(
                spacing: 20.0,
                direction: Axis.vertical,
                alignment: WrapAlignment.spaceAround,
                runAlignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.orange[900], width: 3.0),
                    ),
                    padding: EdgeInsets.all(18.0),
                    child: Icon(
                      Icons.local_taxi,
                      color: Colors.yellow[900],
                      size: 32.0,
                    ),
                  ),
                  Text(
                    'Recent trips',
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 20.0,
                        color: Colors.grey[700]),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.5,
                    child: Divider(
                      height: 1.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 25.0,
              ),
              ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: moreView
                    ? widget.snapshot.data['completedTripIds'].length
                    : 1,
                itemBuilder: (context, index) {
                  return StreamBuilder(
                    stream: Firestore.instance
                        .collection(Routes.TRIPSS_COLLECTION)
                        .document(widget.snapshot.data['completedTripIds'].keys
                            .toList()[index])
                        .snapshots(),
                    builder: (context, completedTripSnapshot) {
                      return completedTripSnapshot.hasData
                          ? Center(
                              child: this
                                  .getSingleTrip(completedTripSnapshot.data),
                            )
                          : Center(child: CircularProgressIndicator());
                    },
                  );
                },
                separatorBuilder: (context, index) {
                  return Icon(
                    Icons.more_horiz,
                    color: Colors.grey,
                  );
                },
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: Column(
                  children: moreView
                      ? <Widget>[
                          InkWell(
                            child: Icon(Icons.arrow_drop_up),
                            onTap: () {
                              setState(() {
                                moreView = false;
                              });
                            },
                          ),
                          Text(
                            "HIDE",
                            style: TextStyle(
                                fontSize: 10.0, fontWeight: FontWeight.w300),
                          ),
                        ]
                      : <Widget>[
                          InkWell(
                            child: Icon(Icons.more_horiz),
                            onTap: () {
                              setState(() {
                                moreView = true;
                              });
                            },
                          ),
                          Text(
                            "MORE",
                            style: TextStyle(
                                fontSize: 10.0, fontWeight: FontWeight.w300),
                          ),
                        ],
                ),
              ),
            ],
          ),
        ));
  }
}
