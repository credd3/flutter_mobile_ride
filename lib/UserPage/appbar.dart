import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import '../Authentication/auth_page.dart';
import '../routes.dart';
import '../user_details.dart';
import './user_page.dart';

Widget userAppBar(context, user, commentsController, isHomePage) {
  return AppBar(
      title: Text(
        "SapeRides",
      ),
      leading: Padding(
        padding: EdgeInsets.all(10.0),
        child: Container(
          padding: EdgeInsets.all(3.0),
          child: InkWell(
            child: Image.asset('assets/sapeRidesLogo.png'),
            onTap: () {
              if (!isHomePage) {
                Navigator.of(context).pushNamedAndRemoveUntil(Routes.USER_HOME,
                    (Route route) => route.settings.name == Routes.USER_HOME, arguments: {"user": user});
              }
            },
          ),
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
          ),
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.call),
          onPressed: () async {
            UrlLauncher.launch('tel://${user['adminContact']}');
          },
          tooltip: 'Call to Admin',
        ),
        IconButton(
          icon: Icon(Icons.notifications),
          onPressed: () {
            // onClick of notification Button
          },
        ),
        PopupMenuButton(
            icon: Icon(Icons.account_circle),
            itemBuilder: (context) => [
                  PopupMenuItem(
                    value: 1,
                    child: Text(
                      '${user['name']}',
                      style: TextStyle(color: Colors.grey[900]),
                    ),
                    enabled: false,
                  ),
                  PopupMenuItem(
                    value: 2,
                    child: Text("Edit Profile"),
                  ),
                  PopupMenuItem(
                    value: 3,
                    child: Text("LogOut"),
                  ),
                ],
            offset: Offset(0, 100),
            onSelected: (value) {
              switch (value) {
                case 2:
                  Navigator.pushNamed(context, Routes.USER_PROFILE, arguments: {
                    'user': user,
                    'commentsController': commentsController
                  });
                  break;
                case 3:
                  commentsController.dispose();
                  FirebaseAuth.instance.signOut();
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      Routes.DEFAULT_ROUTE, (Route route) => route == null);
                  break;
                default:
              }
            }),
      ]);
}
